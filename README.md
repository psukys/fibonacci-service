# Fibonacci web service

Web service that accepts a number **n** and returns the first **n** Fibonacci numbers, starting from 0.

## Usage

Application can be run as a *Docker* container (port 8000 has to be forwarded):
```bash
docker run -p 8000:8000 registry.gitlab.com/psukys/fibonacci-service
```

By default it uses port 8000, thus the qualifying address would be `localhost:8000`

API documentation is described in [Swagger hub](https://app.swaggerhub.com/apis/psukys/fibonacci-service/1.0.0)

Example service query (with [documented deployment](#deployment)):
```bash
curl localhost:8000/api/fibonacci/first/5
```

## Building

Web service is dependant on *Python 3.6* runtime and *pip* package manager for dependency installation.

To install dependencies for running the application, run:
```bash
pip install -r requirements.txt
```

To install dependencies for testing, run:
```bash
pip install -r test_requirements.txt
```

## Deployment

`Dockerfile` contains the instructions to build a Docker image, run:
```bash
docker build -t image-name .
```

This repository as well supplies a docker image that can be used:
```bash
docker pull registry.gitlab.com/psukys/fibonacci-service
```

## Development

To prepare environment for development, setup virtual environment:
```bash
pip install --user virtualenv
virtualenv -p python3.6 env
```

To enable the environment, run:
```bash
source env/bin/activate
```

Install all of the dependencies from [Building section](#building) in virtual environment.

Python codebase is run against pycodestyle (previously PEP8) linter, to apply, run:
```bash
pycodestyle -v . --exclude=.git,__pycache__,env,.eggs --ignore=E501
```

To run the test suite for this application:
```bash
python -m unittest discover tests
```