import app  # test object
import unittest
import hypothesis.strategies as st
from hypothesis import given, assume


def validate_fibonacci_sequence(seq: list) -> bool:
    """Verifies a Fibonacci sequence is valid

    Arguments:
        seq {list} -- list of numbers

    Returns:
        bool -- True if sequence is valid; else - False
    """
    if len(seq) < 3:
        return any([seq == [0], seq == [0, 1]])

    for i in range(2, len(seq)):
        # sum of two previous elements is equal to current
        if seq[i - 2] + seq[i - 1] != seq[i]:
            return False

    return True


class AppTestCase(unittest.TestCase):
    """Tests for Flask backed endpoints"""
    def setUp(self):
        app.application.testing = True
        self.app = app.application.test_client()

    def test_first_fibonacci_smoke(self):
        """Basic smoke test
        Given:
            input num 5
        Expected:
            Space delimited list of 5 numbers: "0 1 1 2 3"
        """
        expected = '0 1 1 2 3'
        res = self.app.get('/api/fibonacci/first/5')
        self.assertEqual(expected, res.data.decode('utf-8'))

    @given(st.integers(min_value=1, max_value=10**3))
    def test_first_fibonacci_fuzzy_positive(self, num: int):
        """Fuzzy test with several result verifications

        Arguments:
            num {int} -- amount of first Fibonacci numbers to request

        Expected:
            1. There are num fibonacci numbers returned
            2. Sequence starts with "0 1"
            3. Sequence is valid (Fibonacci rule)
        """
        res = self.app.get('/api/fibonacci/first/{}'.format(num))
        fib_numbers = list(map(int, res.data.decode('utf-8').split()))
        self.assertEqual(num, len(fib_numbers))
        if num >= 2:
            # Important verification that sequence starts with [0, 1]
            self.assertEqual([0, 1], fib_numbers[0:2])
        self.assertTrue(validate_fibonacci_sequence(fib_numbers))

    @given(st.integers(max_value=-1))
    def test_first_fibonacci_fuzzy_negative(self, num: int):
        """Fuzzy test with negative numbers only.

        Arguments:
            num {int} -- amount of first fibonacci numbers to request

        Expected:
            1. Appropriate error message is returned
        """
        expected = 'Only positive integers accepted'
        res = self.app.get('/api/fibonacci/first/{}'.format(num))
        res_text = res.data.decode('utf-8')
        self.assertEqual(expected, res_text)

    @given(st.data())
    def test_first_fibonacci_fuzzy_any(self, num: str):
        """Fuzzy test with any input being sent to endpoint

        Arguments:
            num: {str} -- symbol(s) that will be requested as amount of items

        Expected:
            1. Appropriate error message is returned
        """
        expected = 'Only positive integers accepted'
        res = self.app.get('/api/fibonacci/first/{}'.format(num))
        res_text = res.data.decode('utf-8')
        self.assertEqual(expected, res_text)
