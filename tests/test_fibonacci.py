import fibonacci
import unittest
import hypothesis.strategies as st
from hypothesis import given, assume


def validate_fibonacci_sequence(seq: list) -> bool:
    """Verifies a Fibonacci sequence is valid

    Arguments:
        seq {list} -- list of numbers

    Returns:
        bool -- True if sequence is valid; else - False
    """
    if len(seq) < 3:
        return any([seq == [0], seq == [0, 1]])

    for i in range(2, len(seq)):
        # sum of two previous elements is equal to current
        if seq[i - 2] + seq[i - 1] != seq[i]:
            return False

    return True


class FibonacciTestCase(unittest.TestCase):
    """Tests for testing out functionality related to Fibonacci sequences."""
    def test_get_n_first_smoke(self):
        """Basic functionality test
        Input:
            5
        Expected:
            [0, 1, 1, 2, 3]
        """
        res = fibonacci.get_n_first(5)
        self.assertEqual([0, 1, 1, 2, 3], res)

    def test_get_n_first_0(self):
        """0 should return empty list
        Input:
            0
        Expectedd:
            []
        """
        res = fibonacci.get_n_first(0)
        self.assertEqual([], res)

    def test_get_n_first_2(self):
        """2 should return [0, 1]
        Input:
            2
        Expected:
            [0, 1]
        """
        res = fibonacci.get_n_first(2)
        self.assertEqual([0, 1], res)

    @given(st.integers(max_value=-1))
    def test_get_n_first_negative(self, num):
        """Validate that error is thrown on negative numbers
        Expected:
            ValueError
        """
        self.assertRaises(ValueError, fibonacci.get_n_first, num)

    @given(st.integers(min_value=2, max_value=10**3))
    def test_get_n_first_fuzzy_positive(self, num):
        """Validate sequence correctness fuzzily for positive numbers
        Expected:
            valid sequences
        """
        res = fibonacci.get_n_first(num)
        self.assertEqual(num, len(res))
        self.assertTrue(validate_fibonacci_sequence(res))
