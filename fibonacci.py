"""Fibonacci related functions."""


def get_n_first(n: int) -> list:
    """Get n first elements from Fibonacci sequence

    Arguments:
        n {int} -- amount of elements of Fibonacci sequence to take from beginning.

    Returns:
        list -- n elements from Fibonacci sequence from beginning.
    """
    sequence = [0, 1]

    if n < 0:
        raise ValueError('Only positive integers accepted')

    if n == 0:
        return []

    if n <= 2:
        return sequence[:n]

    for _ in range(n - 2):
        sequence.append(sum(sequence[-2:]))

    return sequence
