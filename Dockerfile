FROM python:3.6

LABEL maintainer="Paulius Sukys <paul.sukys@gmail.com>"
LABEL description="Image for running Fibonacci service"

WORKDIR /opt/fibonacci

COPY . .

RUN rm -rf tests

RUN pip install -r requirements.txt

CMD gunicorn --bind 0.0.0.0:8000 wsgi:application