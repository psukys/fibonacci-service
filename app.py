"""Flask application that routes request for getting first n fibonacci numbers."""
from flask import Flask, jsonify
from fibonacci import get_n_first

application = Flask(__name__)


@application.route('/api/fibonacci/first/<num>', methods=['GET'])
def fibonacci(num: str):
    """API endpoint for getting first *num* Fibonacci numbers

    Arguments:
        num {str} -- amount of numbers to retrieve from Fibonacci sequence start
    """
    try:
        if not num.isdigit():
            raise ValueError('Only positive integers accepted')
        sequence = list(map(str, get_n_first(int(num))))
        return ' '.join(sequence), 200
    except ValueError as e:
        return str(e), 406


if __name__ == '__main__':
    application.run()
